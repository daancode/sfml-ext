////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#include <SFML/Engine/Director.hpp>
#include <SFML/Engine/Camera.hpp>

namespace sf
{
    Director::~Director()
    {
        for( auto it = m_sceneStack.rbegin(); it != m_sceneStack.rend(); ++it )
        {
            ( *it )->onExit();
        }
        m_sceneStack.clear();

        for( auto&[id, scene] : m_sceneMap )
        {
            scene->release();
        }
        m_sceneMap.clear();
    }

    void Director::destroy(const SceneId& id)
    {
        m_sceneMap.erase(id);
    }

    void Director::initialize(const SceneId& id, bool inBackground /*= true*/)
    {
        if (auto sceneIt = m_sceneMap.find(id); sceneIt != m_sceneMap.end())
        {
            if( auto scene = sceneIt->second; scene && !scene->m_initialized)
            {
                if( inBackground && m_workers.find(id) == m_workers.end() )
                {
                    m_workers.emplace(scene->m_id, std::async(std::launch::async, &Scene::initialize, scene));
                }
                else
                {
                    scene->initialize();
                    scene->m_initialized = true;
                }
            }
        }
    }

    void Director::release(const SceneId& id, bool inBackground /*= true*/)
    {
        if (auto sceneIt = m_sceneMap.find(id); sceneIt != m_sceneMap.end())
        {
            if( auto scene = sceneIt->second; scene && scene->m_initialized )
            {
                if( inBackground && m_workers.find(id) == m_workers.end() )
                {
                    m_workers.emplace(scene->m_id, std::async(std::launch::async, &Scene::release, scene));
                }
                else
                {
                    scene->release();
                    scene->m_initialized = false;
                }
            }
        }
    }

    void Director::request(RequestType type, const std::string& id)
    {
        m_requests.push_back({ type, id });
    }

    void Director::updateWorkers()
    {
        using namespace std::chrono_literals;
        for (auto it = m_workers.begin(); it != m_workers.end();)
        {
            if (it->second.wait_for(0ms) == std::future_status::ready)
            {
                m_sceneMap[it->first]->m_initialized = !m_sceneMap[it->first]->m_initialized;
                it = m_workers.erase(it);
            }
            else ++it;
        }
    }

    void Director::handleRequests()
    {
        ScenePtr scene;
        for( auto it = m_requests.begin(); it != m_requests.end(); )
        {
            if( scene = nullptr; m_sceneMap.find(it->sceneId) != m_sceneMap.end() )
            {
                scene = m_sceneMap[it->sceneId];
            }

            switch( it->type )
            {
                case Push:
                {
                    if( scene && !scene->m_initialized )
                    {
                        it++;
                        initialize(scene->m_id, true);
                        continue;
                    }

                    if( scene && scene->m_state == Scene::Idle )
                    {
                        if( !m_sceneStack.empty() )
                        {
                            m_sceneStack.back()->onSuspend();
                        }

                        scene->m_state = Scene::Active;
                        scene->onEnter();
                        m_sceneStack.push_back(scene);
                    }
                }
                break;

                case PushExclusive:
                {
                    if( scene && !scene->m_initialized )
                    {
                        it++;
                        continue;
                    }

                    if( scene && scene->m_state == Scene::Idle )
                    {
                        if( m_exclusive )
                        {
                            m_exclusive->m_state = Scene::Idle;
                            m_exclusive->onExit();
                        }

                        if( !m_sceneStack.empty() )
                        {
                            m_sceneStack.back()->onSuspend();
                        }

                        scene->m_state = Scene::Exclusive;
                        scene->onEnter();
                        m_exclusive = scene;
                    }
                }
                break;

                case ClearExclusive:
                {
                    if( m_exclusive )
                    {
                        m_exclusive->m_state = Scene::Idle;
                        m_exclusive->onExit();
                        m_exclusive = nullptr;

                        if( !m_sceneStack.empty() )
                        {
                            m_sceneStack.back()->onResume();
                        }
                    }
                }
                break;

                case Pop:
                {
                    if( !m_sceneStack.empty() )
                    {
                        scene = m_sceneStack.back();
                        scene->m_state = Scene::Idle;
                        scene->onExit();
                        m_sceneStack.pop_back();

                        if( !m_sceneStack.empty() )
                        {
                            m_sceneStack.back()->onResume();
                        }
                    }
                }
                break;
            }

            it = m_requests.erase(it);
        }
    }

    void Director::update(float delta)
    {
        updateWorkers();
        handleRequests();

        if (m_exclusive)
        {
            if (auto mode = m_exclusive->m_mode; mode == Scene::OnlyUpdate || mode == Scene::Default)
            {
                m_exclusive->onUpdate(delta);
            }
        }
        else for (auto rit = m_sceneStack.rbegin(); rit != m_sceneStack.rend(); ++rit)
        {
            if (auto mode = (*rit)->m_mode; mode == Scene::OnlyUpdate || mode == Scene::Default)
            {
                (*rit)->onUpdate(delta);
            }
        }
    }

    void Director::fixedUpdate(float delta)
    {
        if (m_exclusive)
        {
            if (auto mode = m_exclusive->m_mode; mode == Scene::OnlyUpdate || mode == Scene::Default)
            {
                m_exclusive->onFixedUpdate(delta);
            }
        }
        else for (auto rit = m_sceneStack.rbegin(); rit != m_sceneStack.rend(); ++rit)
        {
            if (auto mode = (*rit)->m_mode; mode == Scene::OnlyUpdate || mode == Scene::Default)
            {
                (*rit)->onFixedUpdate(delta);
            }
        }
    }

    void Director::draw(RenderTarget& target, RenderStates states) const
    {
        for( auto scene : m_sceneStack )
        {
            if( scene->m_mode == Scene::OnlyRender || scene->m_mode == Scene::Default )
            {
                auto currentStates = states;
                currentStates.transform *= scene->getTransform();
                scene->draw(target, currentStates);
            }
        }

        if( m_exclusive )
        {
            if( auto mode = m_exclusive->m_mode; mode == Scene::OnlyRender || mode == Scene::Default )
            {
                auto currentStates = states;
                currentStates.transform *= m_exclusive->getTransform();
                m_exclusive->draw(target, currentStates);
            }
        }
    }

    Director::ScenePtr Director::get(const SceneId& id)
    {
        return m_sceneMap[id];
    }

    bool Director::isOnTop(const SceneId& id)
    {
        return !m_sceneStack.empty() && m_sceneStack.back()->m_id == id;
    }
}
