////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#include <SFML/Engine/Logger.hpp>

namespace sf
{
    void Logger::Watcher::reset()
    {
        m_nextId = 0;
    }

    void Logger::setEnabled(bool enabled)
    {
        auto& instance = getInstance();
        std::unique_lock lock(instance.m_mutex);
        instance.m_enabled = enabled;
    }

    void Logger::log(LogType type, const std::string& message)
    {
        auto& instance = getInstance();
        auto logTask = [&instance] (LogType type, const std::string& message)
        {
            // Flush and reset data when current id is out of range.
            {
                std::shared_lock lock(instance.m_mutex);
                if( instance.m_currentId >= instance.m_buffer.size() )
                {
                    update();
                    instance.m_currentId = 0;
                    for( auto& watcher : instance.m_watchers )
                    {
                        watcher->reset();
                    }
                }
            }

            std::unique_lock lock(instance.m_mutex);
            if( instance.m_enabled )
            {
                instance.m_buffer[instance.m_currentId] = std::make_pair(type, message);
                instance.m_currentId++;
            }
        };
        std::async(std::launch::async, logTask, type, message);
    }

    Logger::LogData* Logger::get(int id)
    {
        auto& instance = getInstance();
        std::shared_lock lock(instance.m_mutex);
        if( id >= 0 && id < instance.m_currentId && instance.m_enabled )
        {
            return &instance.m_buffer[id];
        }
        else return nullptr;
    }

    void Logger::removeWatchers()
    {
        getInstance().m_watchers.clear();
    }

    void Logger::update()
    {
        auto& instance = getInstance();
        std::shared_lock lock(instance.m_mutex);
        if( instance.m_enabled )
        {
            for( auto& watcher : instance.m_watchers )
            {
                if( auto log = get(watcher->m_nextId); log )
                {
                    watcher->print(log->first, log->second);
                    watcher->m_nextId++;
                }
            }
        }
    }

    bool Logger::isEnabled()
    {
        return getInstance().m_enabled;
    }

    void Log::timestamp(std::stringstream& stream)
    {
        auto time_point = std::chrono::system_clock::now();
        auto ttp = std::chrono::system_clock::to_time_t(time_point);
        stream << "[" << std::put_time(std::localtime(&ttp), "%T") << "]";
    }
}
