////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#include <SFML/Engine/Animation.hpp>

namespace sf
{
    void Animation::instantiate(Sprite& sprite, Prefab& prefab)
    {
        reset();
        m_sprite = &sprite;
        m_prefab = &prefab;
        m_reversed = prefab.m_reversed;
        m_loop = prefab.m_loop;
    }

    void Animation::setLoop(bool loop)
    {
        m_loop = loop;
    }

    void Animation::setReversed(bool reversed)
    {
        m_reversed = reversed;
    }

    void Animation::play()
    {
        if( !m_playing && m_prefab)
        {
            m_stopRequested = false;
            m_currentFrame = m_reversed ? m_prefab->m_frames.size() - 1 : 0;
            m_frameTime = 0.f;
            m_playing = true;

            if( m_sprite )
            {
                m_sprite->setTextureRect(m_prefab->m_frames[m_currentFrame].rect);
            }
        }
    }

    bool Animation::isPlaying()
    {
        return m_playing;
    }

    void Animation::stop(bool immediately)
    {
        m_stopRequested = true;
        if( immediately && m_playing )
        {
            m_playing = false;
        }
    }

    void Animation::update(float delta)
    {
        if( m_playing )
        {
            m_frameTime += delta;

            if( m_prefab && m_frameTime >= m_prefab->m_frames[m_currentFrame].duration )
            {
                m_currentFrame += m_reversed ? -1 : 1;

                if( m_currentFrame < 0 || m_currentFrame >= m_prefab->m_frames.size() )
                {
                    if( !m_loop || m_stopRequested )
                    {
                        m_playing = false;
                    }
                    else if( m_loop )
                    {
                        m_currentFrame = m_reversed ? m_prefab->m_frames.size() - 1 : 0;
                    }
                }

                if( m_playing && m_sprite )
                {
                    m_sprite->setTextureRect(m_prefab->m_frames[m_currentFrame].rect);
                }

                m_frameTime = 0.f;
            }
        }
    }

    void Animation::reset()
    {
        m_frameTime = 0.f;
        m_stopRequested = false;
        m_playing = false;

        if( m_prefab )
        {
            m_currentFrame = m_reversed ? m_prefab->m_frames.size() - 1 : 0;

            if( m_sprite )
            {
                m_sprite->setTextureRect(m_prefab->m_frames[m_currentFrame].rect);
            }
        }
    }
}
