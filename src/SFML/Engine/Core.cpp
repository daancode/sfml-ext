////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#include <SFML/Engine/Core.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Engine/Director.hpp>
#include <SFML/Engine/Registry.hpp>
#include <SFML/Engine/Debug.hpp>
#include <SFML/Engine/Camera.hpp>
#include <SFML/Engine/Services.hpp>

namespace sf::constants
{
    static constexpr auto TimeStep{ 0.02f };
}

namespace sf
{
    void Engine::FrameTime::update()
    {
        delta = timer.restart().asSeconds();
        accumulator += delta;
        accumulator = std::clamp(accumulator, 0.f, maxUpdateTime);
        frameTime += delta;
    }

    bool Engine::FrameTime::needFixedUpdate()
    {
        if (accumulator > constants::TimeStep)
        {
            accumulator -= constants::TimeStep;
            return true;
        }
        else return false;
    }

    void Engine::FrameTime::refreshFpsCounter(Debug* debug)
    {
        if (++frame; debug && frameTime >= 1.f)
        {
            debug->clear();
            debug->print({ 0, 0 }, "FPS: " + std::to_string(frame));
            frameTime = 0.f;
            frame = 0;
        }
    }

    void Engine::run(std::function<void()> initializer)
    {
        Services::Data servicesData;
        Services::initialize(servicesData);

        Logger::setEnabled(true);

        Services::createAndLock<Director>();
        Services::createAndLock<RenderWindow>();

#ifdef SFML_DEBUG
        Services::create<Debug>();
#endif

        // User defined initialization.
        initializer();

        auto debug = Services::get<Debug>();
#ifdef SFML_DEBUG
        if ( debug )
        {
            debug->init();
            debug->setEnabled(true);
            Logger::addWatcher<Debug::Watcher>(debug->getConsoleScene());
        }
#endif

        FrameTime time;
        EventHandler eventHandler;

        auto window = Services::get<RenderWindow>();
        auto director = Services::get<Director>();

        while (window->isOpen())
        {
            time.update();
            time.refreshFpsCounter(debug);

            // Process events.
            {
                eventHandler.fetch(*window);

                if( auto ev = eventHandler.get(sf::Event::Closed); ev )
                {
                    window->close();
                }

                if( auto ev = eventHandler.get(sf::Event::Resized); ev )
                {
                    if( auto camera = Services::get<Camera>(); camera )
                    {
                        camera->update();
                    }
                }

                if( auto ev = eventHandler.get(sf::Event::KeyPressed); debug && ev )
                {
                    if( ev->key.code == sf::Keyboard::F2 )
                    {
                        debug->setMode(sf::Debug::Mode::Overlay);
                    }

                    if( ev->key.code == sf::Keyboard::F3 )
                    {
                        debug->setMode(sf::Debug::Mode::Console);
                    }
                }
            }

            director->update(time.delta);

            while(time.needFixedUpdate())
            {
                director->fixedUpdate(constants::TimeStep);
            }

            Logger::update();

            window->clear();
            window->draw(*director);
            if (debug)
            {
                window->draw(*debug);
            }
            window->display();
        }

        Logger::removeWatchers();
        Services::release(true);
    }
}
