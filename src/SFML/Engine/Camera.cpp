////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#include <SFML/Engine/Camera.hpp>

namespace sf
{
    void Camera::setEnabled(bool enabled)
    {
        m_enabled = enabled;
    }

    void Camera::setRenderWindow(RenderWindow* window)
    {
        if (m_window)
        {
            m_window->setView(m_window->getDefaultView());
        }

        m_window = window;
        m_needUpdate = true;
    }

    void Camera::setVirtualResolution(Vector2f resolution)
    {
        m_virtualResolution = resolution;
        m_needUpdate = true;
    }

    void Camera::keepAspectRatio(bool keepRatio)
    {
        m_keepAspectRatio = keepRatio;
        m_needUpdate = true;
    }

    void Camera::set()
    {
        if (m_enabled && m_window)
        {
            if (m_needUpdate)
            {
                m_view.setSize(m_virtualResolution);
                m_view.setCenter({ m_virtualResolution.x / 2,
                                   m_virtualResolution.y / 2 });

                if (m_keepAspectRatio)
                {
                    auto windowRatio = m_window->getSize().x / (float)m_window->getSize().y;
                    auto viewRatio = m_virtualResolution.x / m_virtualResolution.y;

                    if (windowRatio > viewRatio)
                    {
                        auto size = viewRatio / windowRatio;
                        m_view.setViewport({ (1 - size) / 2.f, 0.f, size, 1.f });
                    }
                    else
                    {
                        auto size = windowRatio / viewRatio;
                        m_view.setViewport({ 0.f, (1 - size) / 2.f, 1.f, size });
                    }
                }
                
                m_needUpdate = false;
            }

            m_window->setView(m_view);
        }
    }

    void Camera::update()
    {
        m_needUpdate = true;
    }

    void Camera::restore()
    {
        if (m_window)
        {
            m_window->setView(m_window->getDefaultView());
        }
    }

    const Vector2f Camera::getVirtualResolution() const
    {
        return m_virtualResolution;
    }

    bool Camera::isEnabled() const
    {
        return m_enabled && m_window;
    }
}
