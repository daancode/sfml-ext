////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#include <SFML/Engine/Debug.hpp>
#include <SFML/Engine/Director.hpp>
#include <SFML/Engine/Camera.hpp>

namespace sf
{
    bool Debug::CommandLine::initialize()
    {
        if( auto window = Services::get<RenderWindow>(); window )
        {
            auto size = window->getSize();

            m_buffer.create(size.x / 9, size.y / 14 - 1);
            m_cmd.create(size.x / 9, 1);
            m_cmd.setPosition(0.f, ( float ) size.y - 14.f);
            m_totalCmdSize = size.x / 9u;

            m_background.setSize(static_cast< Vector2f >( size ));
            m_background.setFillColor(Color(0, 0, 0, 128));

            return true;
        }
        else return false;
    }

    void Debug::CommandLine::onEnter()
    {
        m_cmd.clear();
        m_cmdBuffer = "> ";
        m_cmd.print(m_cmdBuffer);
    }

    void Debug::CommandLine::onUpdate(float delta)
    {
        EventHandler events;

        auto debug = Services::get<Debug>();

        if (auto ev = events.get(sf::Event::KeyPressed); ev && debug )
        {
            if (ev->key.code == sf::Keyboard::Escape)
            {
                debug->setMode(Debug::Mode::Overlay);
            }
        }

        if (auto ev = events.get(sf::Event::TextEntered); ev && debug )
        {
            if (char c = ev->text.unicode; c > 31 && c <= 127)
            {
                if (m_cmdBuffer.size() < m_totalCmdSize)
                {
                    m_cmdBuffer.push_back(c);
                }
            }
            else if (c == 8 && m_cmdBuffer.size() > 2)
            {
                m_cmdBuffer.pop_back();
            }
            else if (c == 13)
            {
                if (!debug->executeCommand(m_cmdBuffer.substr(2, m_cmdBuffer.size())))
                {
                    m_buffer.print("Command not found.\n");
                }

                m_cmdBuffer = "> ";
            }

            m_cmd.clear();
            m_cmd.print(m_cmdBuffer);
        }
    }

    void Debug::CommandLine::draw(RenderTarget& target, RenderStates states) const
    {
        target.draw(m_background, states);
        target.draw(m_buffer, states);
        target.draw(m_cmd, states);
    }

    void Debug::CommandLine::print(const std::string& message)
    {
        m_buffer.print(message);
    }

    void Debug::CommandLine::print(const std::string& message, Color color)
    {
        auto oldColor = m_buffer.getForegroundColor();
        m_buffer.setForegroundColor(color);
        m_buffer.print(message);
        m_buffer.setForegroundColor(oldColor);
    }

    Debug::Watcher::Watcher(CommandLine* console)
        : m_console(console)
    {}

    void Debug::Watcher::print(sf::LogType type, std::string_view message)
    {
        if( m_console )
        {
            Color color{ Color::White };
            switch( type )
            {
                case LogType::Warning: color = Color(255, 150, 0); break;
                case LogType::Error: color = Color::Red; break;
            }
            m_console->print(std::string{ message }, color);
        }
    }

    void Debug::init()
    {
        if( auto camera = Services::get<Camera>(); camera && camera->isEnabled() )
        {
            m_size = static_cast<Vector2u>( camera->getVirtualResolution() );
        }
        else if( auto window = Services::get<RenderWindow>(); window )
        {
            m_size = window->getSize();
        }

        m_overlay.setForegroundColor(sf::Color::White);
        m_overlay.setBackgroundColor(sf::Color::Black);
        m_overlay.create(m_size.x / 9, m_size.y / 14);

        if ( auto director = Services::get<Director>(); director )
        {
            director->release(constants::DebugScene);
            director->create<CommandLine>(constants::DebugScene);
            director->initialize(constants::DebugScene, false);
        }
    }

    void Debug::setEnabled(bool enabled)
    {
        if (m_enabled = enabled; !m_enabled)
        {
            if( auto director = Services::get<Director>(); director )
            {
                if( director->get(constants::DebugScene)->getState() == Scene::Exclusive )
                {
                    director->request(Director::ClearExclusive);
                    m_mode = Mode::Overlay;
                }
            }
        }
    }

    bool Debug::isEnabled() const
    {
        return m_enabled;
    }

    void Debug::setMode(Mode mode)
    {
        if ( auto director = Services::get<Director>(); director && m_enabled)
        {
            switch( m_mode = mode; m_mode )
            {
                case Mode::Overlay:
                {
                    if (director->get(constants::DebugScene)->getState() == Scene::Exclusive)
                    {
                        director->request(Director::ClearExclusive);
                    }
                }
                break;

                case Mode::Console:
                {
                    director->request(Director::PushExclusive, constants::DebugScene);
                }
                break;
            }
        }
    }

    void Debug::print(const Vector2i& pos, const std::string& string)
    {
        if (m_enabled)
        {
            auto oldCursor = m_overlay.getCursor();
            m_overlay.setCursor(pos);
            m_overlay.print(string);
            m_overlay.setCursor(oldCursor);
        }
    }

    void Debug::draw(RenderTarget& target, RenderStates states) const
    {
        if (m_enabled && m_mode == Mode::Overlay)
        {
            target.draw(m_overlay, states);
        }
    }

    void Debug::clear()
    {
        m_overlay.clear();
    }

    void Debug::addCommand(const Key& cmd, Callback callback)
    {
        m_cmdMap.insert({ cmd, callback });
    }

    bool Debug::executeCommand(const Key& cmd)
    {
        std::istringstream iss(cmd);

        Arguments args
        {
            std::istream_iterator<std::string>{iss},
            std::istream_iterator<std::string>{}
        };

        if (!args.empty())
        {
            if (auto cmdIt = m_cmdMap.find(args[0]); cmdIt != m_cmdMap.end())
            {
                args.erase(args.begin());
                cmdIt->second(args);
                return true;
            }
        }

        return false;
    }

    void Debug::removeCommand(const Key& cmd)
    {
        m_cmdMap.erase(cmd);
    }

    Debug::CommandLine* Debug::getConsoleScene()
    {
        if( auto director = Services::get<Director>(); director)
        {
            auto scene = director->get(constants::DebugScene);
            return std::dynamic_pointer_cast<CommandLine>( scene ).get();
        }
        else return nullptr;
    }
}
