////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#include <SFML/Engine/Vfs.hpp>

#include <array>
#include <fstream>
#include <sstream>

#ifdef __clang__
    #include <experimental/filesystem>
    namespace os = std::experimental::filesystem;
#else
    #include <filesystem>
    namespace os = std::filesystem;
#endif

namespace sf
{
    void Vfs::Cipher::encrypt( std::vector<Byte>& data, const Key& key )
    {
        if( !key.empty() )
        {
            for( auto i = 0u; i < data.size(); ++i )
            {
                data[i] ^= key[i %  key.size()];
            }
        }
    }

    void Vfs::build( const Path& path, const Path& output, const Key& key )
    {
        m_fileMap.clear();
        
        if( std::ofstream stream( output, std::ios::binary ); stream.is_open() )
        {
            loadFromDirectory( path );

            std::stringstream header;
            header << "sfpak " << m_fileMap.size() << "\n";
            for( auto&[key, value] : m_fileMap )
            {
                header << key << " " << value->file.size() << " " << value->lastModification << "\n";
            }

            std::vector<Byte> pak( { std::istreambuf_iterator<char>( header ),
                                   std::istreambuf_iterator<char>() } );

            for( auto&[key, value] : m_fileMap )
            {
                pak.insert( pak.end(), value->file.begin(), value->file.end() );
            }

            m_encryption->encrypt( pak, key );

            stream.write( ( char* ) pak.data(), pak.size() );
            stream.close();
        }

        m_fileMap.clear();
    }

    void Vfs::loadFromFile( const Path& path )
    {
        if( std::ifstream stream( path, std::ios::binary ); stream.is_open() )
        {
            auto handle = std::make_shared<FileHandle>();
            handle->path = path;
            auto size = static_cast< size_t >( os::file_size( path ) );
            handle->file.resize( size );
            stream.read( ( char* ) handle->file.data(), size );
            handle->lastModification = os::last_write_time( os::path( path ) ).time_since_epoch().count();
            m_fileMap.insert_or_assign( handle->path, handle );
            stream.close();
        }
    }

    void Vfs::loadFromDirectory( const Path& path )
    {
        if( os::path directory( path ); exists( directory ) && is_directory( directory ) )
        {
            for( auto& entry : os::recursive_directory_iterator( directory ) )
            {
                if( os::is_regular_file( entry ) )
                {
                    loadFromFile( entry.path().generic_string() );
                }
            }
        }
    }

    void Vfs::loadFromPak( const Path& path, const Key& key )
    {
        std::vector<Byte> pak;
        if( std::ifstream stream( path, std::ios::binary ); stream.is_open() )
        {
            auto size = static_cast< size_t >( os::file_size( path ) );
            pak.resize( size );
            stream.read( ( char* ) pak.data(), size );
            m_encryption->encrypt( pak, key );
        }

        if( !pak.empty() )
        {
            std::string format;
            std::stringstream stream( std::string( pak.begin(), pak.end() ) );

            if( stream >> format && format == "sfpak" )
            {
                int fileTableSize{ 0 };
                stream >> fileTableSize;
                std::vector<std::shared_ptr<FileHandle>> files;

                for( int i = 0; i < fileTableSize; ++i )
                {
                    size_t fileSize{ 0u };
                    auto file = std::make_shared<FileHandle>();
                    file->pak = path;
                    stream >> file->path >> fileSize >> file->lastModification;
                    file->file.resize( fileSize );
                    files.push_back( file );
                }

                stream.seekg( 1, std::ios::cur );

                for( int i = 0; i < fileTableSize; ++i )
                {
                    stream.read( ( char* ) files[i]->file.data(), files[i]->file.size() );
                    m_fileMap.insert_or_assign( files[i]->path, files[i] );
                }
            }
        }
    }

    std::shared_ptr<const Vfs::FileHandle> Vfs::get( const Path& path )
    {
        if( auto handle = m_fileMap.find( path ); handle != m_fileMap.end() )
        {
            return handle->second;
        }
        else return nullptr;
    }

    void Vfs::release( const Path& path )
    {
        m_fileMap.erase( path );
    }
}
