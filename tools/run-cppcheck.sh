#!/bin/bash

apt-get install -y cppcheck

cppcheck --enable=all src/ 2> result.log
cat result.log

ERRORS=$(wc -l < result.log );
echo "$ERRORS problems detected.";