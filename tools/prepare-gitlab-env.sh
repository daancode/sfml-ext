#!/bin/bash

apt-get update -y
apt-get install libx11-dev -y
apt-get install libxi-dev -y
apt-get install libxrandr-dev -y
apt-get install libxcb1-dev -y
apt-get install libx11-xcb-dev -y
apt-get install libxcb-randr0-dev -y
apt-get install libxcb-image0-dev -y
apt-get install libgl1-mesa-dev -y
apt-get install libudev-dev -y
apt-get install libfreetype6-dev -y
apt-get install libjpeg-dev -y
apt-get install libopenal-dev -y
apt-get install libflac-dev -y
apt-get install libvorbis-dev -y
apt-get install cmake -y