////////////////////////////////////////////////////////////
/// \mainpage
///
/// \section example Getting started
/// Here is a short example, to show you how simple it is to use this library:
///
/// \code
/// #include <SFML/Engine.hpp>
/// 
/// class MyScene : public sf::Scene
/// {
/// public:
///     void initialize() override
///     {
///         m_shape.setSize({ 100, 100 });
///         m_shape.setFillColor(sf::Color::Red);
///         m_shape.setOrigin({ 50, 50 });
///         m_shape.setPosition({ 683, 384 });
///     }
///
///     void onUpdate(float delta) override
///     {
///         m_shape.rotate(90.f * delta);
///     }
///
///     void draw(sf::RenderTarget& target, sf::RenderStates states) const override
///     {
///         target.draw(m_shape, states);
///     }
///
/// private:
///     sf::RectangleShape m_shape;
/// };
///
/// int main()
/// {
///     sf::Engine::run([](sf::Services& services)
///     {
///         services.debug->setEnabled(true);
///
///         services.window->create({ 1920, 1080, 32 }, "Window Title");
///
///         services.camera->setEnabled(true);
///         services.camera->setVirtualResolution({ 1366, 768 });
///         services.camera->keepAspectRatio(true);
///
///         services.director->create<MyScene>("SceneTag");
///         services.director->initialize("SceneTag", false);
///         services.director->request(sf::Director::Push, "SceneTag");
///     });
/// }
/// \endcode
////////////////////////////////////////////////////////////
