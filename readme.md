# Game development extension for SFML

<img align="right" src="https://i.imgur.com/iXl9Ynv.png" width="100" height="100"/>  

Game development utilities build on top of [SFML](https://sfml-dev.org) project.  
With this extension you can focus on developing your game without thinking about some basic things  
which not included to sfml by default like scene management, animations etc.  
For details you should look to [API documentation](https://daancode.gitlab.io/sfml-ext).

## Requirements

- SFML requirements
- Compiler support for C++17

## Key features

- Game loop wrapper
- More flexible event handling
- Scene management
- Custom built-in encrypted pak file format
- Aspect ratio handling
- Logging utility
- Debug console with command line
- Registry for holding key-value pairs
- Sprite sheet animation system

## Future plans

- Basic UI module
- Entity-component system
- Particles

## Install

Follow the instructions of the [tutorials](https://www.sfml-dev.org/tutorials/), there is one for each platform/compiler that SFML supports.

## Getting started

In *sfml-ext* your code is divided into scenes. Each scene should represent different part of your game eg. splash, main menu or game scene. Scene interface provide some methods which your scene implementation can override. This methods will cover basically all game loop sections and transition between scenes. For example if you want to save progress when new scene will be pushed to the scene stack you can do this in `onSuspend()` method which will be called before pushing new scene. Detailed informations you can get in API documentation.

```cpp
#include <SFML/Engine.hpp>

class ExampleScene : public sf::Scene
{
public:
    void initialize() override
    {
        m_shape.setSize({ 100, 100 });
        m_shape.setPosition({ 512, 384 });
        m_shape.setOrigin({ 50, 50 });
        m_shape.setFillColor(sf::Color::Cyan);
    }

    void onUpdate(float delta) override
    {
        sf::EventHandler events;
        if(auto ev = events.get(sf::Event::KeyPressed); ev)
        {
            if(ev->key.code == sf::Keyboard::Escape)
            {
                // Scene have directly access to all services by
                // shared pointers.
                window->close();
            }
        }

        m_shape.rotate(delta * 90.f);
    }

    void draw(sf::RenderTarget& target, sf::RenderStates states) const override
    {
        target.draw(m_shape, states);
    }

private:
    sf::RectangleShape m_shape;
};

int main()
{
   sf::Engine::run([](sf::Services& services)
   {
        // Debug utility allow you to print variables on the sceen or
        // create debug commands with debug console scene.
        // Yeah, yo don't need to use std::cout anymore !
        services.debug->setEnabled(true);

        services.window->create({ 1366, 768, 32 }, "My first sfml-ext game.");

        // Camera utility will handle displaying your game in different resolutions
        // than you specify here. For example our window is different than virtual resolution
        // so camera will center your scene view and draw black rectangles on sides.
        services.camera->setEnabled(true);
        services.camera->setVirtualResolution({ 1024, 768 });

        // This parameter define if you allow camera to strech your content. 
        services.camera->keepAspectRatio(true);

        // You must specify tag for your scene, you can also pass scene 
        // constructor arguments after your tag.
        services.director->create<ExampleScene>("ExampleTag" /*, arg1, arg2, etc... */);
       
        // If you set second parameter to true, scene will be initialized in
        // separate thread.
        // This allow you to show loading bar while your main scene will be 
        // initialized. Pretty cool stuff.
        services.director->initialize("ExampleTag", false);

        // Scene will be pushed if initialization is done, otherwise it will
        // ignore this request in current iteration and try again later.
        services.director->request(sf::Director::Push, "ExampleTag");
   });
}
``` 

If you compile code above, you should see cyan rectangle rotating at the center of your window.  
**Welcome to the sfml-ext :) !**
