////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_ENGINE_SERVICES_HPP
#define SFML_ENGINE_SERVICES_HPP

#include <any>
#include <memory>
#include <set>
#include <algorithm>
#include <vector>

#include <SFML/Engine/Export.hpp>

namespace sf
{
    class SFML_ENGINE_API Services
    {
    public:
        using Data = std::vector<std::any>;

        template <typename _Type>
        using Provider = std::shared_ptr<_Type>;

        Services() = delete;
        Services(const Services&) = delete;
        Services(Services&&) = delete;
        Services& operator=(const Services&) = delete;
        Services& operator=(Services&&) = delete;

        static void initialize(Data& services);

        template <typename _Type>
        static bool find()
        {
            if( s_services )
            {
                for( auto& service : *s_services )
                {
                    if( service.type().hash_code() == typeid( Provider<_Type> ).hash_code() )
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        template <typename _Type, typename... _Args>
        static void create(_Args&& ...args)
        {
            if( s_services && !find<_Type>() )
            {
                s_services->emplace_back(std::make_any<Provider<_Type>>(new _Type(std::forward<_Args>(args)...)));
            }
        }

        template <typename _Type, typename... _Args>
        static void createAndLock(_Args&& ...args)
        {
            create<_Type>(std::forward<_Args>(args)...);
            lock<_Type>();
        }

        template <typename _Type>
        static void lock()
        {
            s_locks.insert(typeid( Provider<_Type> ).hash_code());
        }

        template <typename _Type>
        static _Type* get()
        {
            if( s_services )
            {
                for( auto& service : *s_services )
                {
                    if( service.type().hash_code() == typeid( Provider<_Type> ).hash_code() )
                    {
                        return std::any_cast<Provider<_Type>>( service ).get();
                    }
                }
            }
            return nullptr;
        }

        template <typename _Type>
        static void destroy()
        {
            if( s_locks.find(typeid( Provider<_Type> ).hash_code()) == s_locks.end() && s_services )
            {
                auto TypeMatch = [] (std::any& service) -> bool
                {
                    return service.type().hash_code() == typeid( Provider<_Type> ).hash_code();
                };

                auto removeIt = std::remove_if(s_services->begin(), s_services->end(), TypeMatch);
                s_services->erase(removeIt, s_services->end());
            }
        }

        static void release(bool destroyServices = false);

    private:
        inline static Data* s_services{ nullptr };
        static std::set<size_t> s_locks;
    };
}

#endif // SFML_ENGINE_SERVICES_HPP
