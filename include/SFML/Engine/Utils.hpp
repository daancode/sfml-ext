////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_ENGINE_UTILS_HPP
#define SFML_ENGINE_UTILS_HPP

#include <SFML/Engine/Export.hpp>

#include <vector>
#include <sstream>

namespace sf::utils
{
    inline SFML_ENGINE_API std::vector<std::string> split(const std::string& str, char delim)
    {
        std::vector<std::string> tokens;
        std::stringstream stream(str);
        for( std::string token; std::getline(stream, token, delim); )
        {
            if( !token.empty() )
            {
                tokens.emplace_back(token);
            }
        }
        return tokens;
    }
}

#endif // SFML_ENGINE_UTILS_HPP
