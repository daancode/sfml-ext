////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_ENGINE_EVENT_HANDLER_HPP
#define SFML_ENGINE_EVENT_HANDLER_HPP

#include <SFML/Engine/Export.hpp>
#include <SFML/Window.hpp>

#include <vector>

namespace sf
{
    /*!
    *  \brief Utility class for handle basic SFML events and propagate it to any place.
    */
    class SFML_ENGINE_API EventHandler
    {
        friend class Engine;

    public:
        /*!
            \brief Default destructor, release exclusive handler.
        */
        ~EventHandler();

        /*!
            \brief Enable or disable event handling for this handler.

            \param enable True to enable, false to disable
        */
        void setEnabled(bool enable);

        /*!
            \brief Enable or disable exclusive handling for this handler.

            \param exclusive True to set, false to clear
        */
        void setExclusive(bool exclusive);

        /*!
            \brief Get event pointer for given type.

            \param type Event type id

            \return Pointer to event
        */
        Event* get(Event::EventType type);

    private:
        /*!
            \brief Fetch events from window, should be called once per frame.

            \param window Reference to window
        */
        static void fetch(Window& window);

    private:
        static std::vector<Event> m_eventBuffer;        ///> Static event buffer
        static EventHandler*      m_exclusiveHandler;   ///> Pointer to exclusive handler
        bool                      m_isEnabled{ true };  ///> Flag for detect if handler is enabled
    };
}

#endif // SFML_ENGINE_EVENT_HANDLER_HPP

/*!
    \class sf::EventHandler
    \ingroup engine

    [TODO] Detailed description ... maybe later :)
*/
