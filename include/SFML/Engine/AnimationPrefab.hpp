////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_ENGINE_ANIMATION_PREFAB_HPP
#define SFML_ENGINE_ANIMATION_PREFAB_HPP

#include <SFML/Engine/Export.hpp>
#include <SFML/Graphics/Rect.hpp>

#include <vector>

namespace sf
{
    /*!
        \brief Class for holding predefined animation data.
    */
    class AnimationPrefab
    {
        friend class Animation;

        /*!
            \brief Struct for define single animation frame.
        */
        struct Frame
        {
            IntRect rect;   ///< Sprite texture rect.
            float duration; ///< Frame duration.
        };

    public:
        /*!
            \brief Enable or disable animation looping.

            \param loop If true animation will be playing in loop
        */
        void setLoop(bool loop);

        /*!
            \brief Enable or disable playing animation in reverse frame order.

            \param reversed If true animation will be playing in reversed order
        */
        void setReversed(bool reversed);

        /*!
            \brief Add new animation frame.

            \param rect Texture rect which defines frame
            \param duration This frame duration
        */
        void addFrame(IntRect rect, float duration);

    private:
        std::vector<Frame>  m_frames;            ///< Vector containing frames.
        bool                m_reversed{ false }; ///< Flag indices reversed animation.
        bool                m_loop{ false };     ///< Flag indices looping animation.
    };
}

#endif // SFML_ENGINE_ANIMATION_PREFAB_HPP

/*!
    \class sf::AnimationPrefab
    \ingroup engine

    [TODO] Maybe later ... :)
*/
