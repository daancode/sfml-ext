////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_ENGINE_CORE_HPP
#define SFML_ENGINE_CORE_HPP

#include <functional>
#include <memory>

#include <SFML/System/Clock.hpp>
#include <SFML/Engine/Export.hpp>
#include <SFML/Engine/Logger.hpp>
#include <SFML/Engine/Registry.hpp>
#include <SFML/Engine/Services.hpp>

namespace sf
{
    class Debug;

    /*!
        \brief Game loop wrapper. 
    */
    class SFML_ENGINE_API Engine
    {
        struct FrameTime
        {
            void update();

            bool needFixedUpdate();

            void refreshFpsCounter(Debug* debug);

            Clock   timer;
            float   delta{ 0.f };
            float   accumulator{ 0.f };
            float   maxUpdateTime{ 1.f };
            int     frame{ 0 };
            float   frameTime{ 0.f };
        };

    public:
        /*!
            \brief Prepare and run game loop.

            \param initializer Service initialization method
        */
        static void run(std::function<void()> initializer);
    };
}

#endif // SFML_ENGINE_CORE_HPP

/*!
    \class sf::Engine
    \ingroup engine

    Entry point for new project. Allow you to prepare game scenes,
    create window etc. It's wrap basic SFML loop into single method
    and propagate each section like update and render to scene interface.

    \code
    #include <SFML/Engine.hpp>

    int main()
    {
        sf::Engine::run([](auto& context)
        {
            auto&[window, director] = context;

            window.create({ 1024, 768, 32 }, "Window Title");

            director.create<ExampleScene>("Example");
            director.initialize("Example", false);

            director.push("Example");
        });
    }
    \endcode
*/
