////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_ENGINE_ANIMATION_HPP
#define SFML_ENGINE_ANIMATION_HPP

#include <SFML/Engine/Export.hpp>
#include <SFML/Engine/AnimationPrefab.hpp>
#include <SFML/Graphics.hpp>

#include <map>

namespace sf
{
    /*!
        \brief Class for handle sprite animation.
    */
    class Animation
    {
        friend class AnimationPrefab;

        using Prefab = AnimationPrefab;

    public:
        /*!
            \brief Create animation from sprite and animation prefab.

            \param sprite Reference to sprite instance, must exist as long as animation
            \param prefab Reference to prefab instance, must exist as long as animation

            \see sf::AnimationPrefab
        */
        void instantiate(Sprite& sprite, Prefab& prefab);

        /*!
            \brief Enable or disable animation looping.

            \param loop If true animation will be playing in loop
        */
        void setLoop(bool loop);

        /*!
            \brief Enable or disable playing animation in reverse frame order.

            \param reversed If true animation will be playing in reversed order
        */
        void setReversed(bool reversed);

        /*!
            \brief Play animation.
        */
        void play();

        /*!
            \brief Check if this animation is playing now.

            \return true If animation is playing, false otherwise
        */
        bool isPlaying();

        /*!
            \brief Stop playing animation.

            \param immediately If false, animation will stop after last frame 
        */
        void stop(bool immediately = false);

        /*!
            \brief Update current frame and sprite texture rect.

            \param delta Last frame time 
        */
        void update(float delta);

        /*!
            \brief Reset state of animation.
        */
        void reset();

    private:
        Prefab* m_prefab{ nullptr };        ///< Pointer to prefab instance.
        Sprite* m_sprite{ nullptr };        ///< Pointer to sprite instance.
        size_t  m_currentFrame{ 0 };        ///< Current frame index.
        float   m_frameTime{ 0.f };         ///< Current frame elapsed time.
        bool    m_reversed{ false };        ///< Flag indices reversed animation.
        bool    m_stopRequested{ false };   ///< Flag indices if animation should stop.
        bool    m_playing{ false };         ///< Flag indices if animation is playing.
        bool    m_loop{ false };            ///< Flag indices loop animation.
    };
}

#endif // SFML_ENGINE_ANIMATION_HPP

/*!
    \class sf::Animation
    \ingroup engine

    [TODO] Maybe later ... :)
*/