////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_ENGINE_CONSOLE_HPP
#define SFML_ENGINE_CONSOLE_HPP

#include <SFML/Engine/Export.hpp>
#include <SFML/Graphics.hpp>

#include <string>
#include <variant>
#include <vector>
#include <sstream>
#include <set>
#include <map>

namespace sf
{
    /*!
        \brief Drawable console with it's own transform.
    */
    class SFML_ENGINE_API Console : public Drawable, public Transformable
    {
    public:
        /*!
            \brief Constructor. Load embedded font from memory.
        */
        Console();

        /*!
            \brief Create console and calculate size.
        */
        void create(int charactersInRow, int charactersInColumn);

        /*!
            \brief Set foreground cell color.

            \param foreground Text color
        */
        void setForegroundColor(Color foreground);

        /*!
            \brief Set background cell color.

            \param background Background cell color
        */
        void setBackgroundColor(Color background);

        /*!
            \brief Scroll console by given offset (in rows).

            \param offset Offset which should be applied
        */
        void scroll(int offset);

        /*!
            \brief Set scroll to given offset.

            \param offset Offset which console should be set
        */
        void setScroll(int offset);

        /*!
            \brief Set new cursor position.

            \param cursor New position of cursor
        */
        void setCursor(const Vector2i& cursor);

        /*!
            \brief Get current cursor position.

            \return Current cursor position
        */
        const Vector2i& getCursor();

        /*!
            \brief Print values to surface.

            \param args Values that should be displayed
        */
        template <typename... _Args>
        void print(_Args&& ...args)
        {
            std::stringstream stream;
            (stream << ... << args);
            drawOnSurface(stream.str());
        }

        /*!
            \brief Clear current console surface.
        */
        void clear();

        /*!
            \brief Print current surface to target.

            \param target Render target
            \param states Current render states
        */
        void draw(RenderTarget& target, RenderStates states) const;

        /*!
            \brief Get foreground cell color.

            \param Current foreground cell color
        */
        Color getForegroundColor();

        /*!
            \brief Get background cell color.

            \return Current background cell color
        */
        Color getBackgroundColor();

    private:
        /*!
            \brief Draw text on surface.

            \param buffer Buffer to render
        */
        void drawOnSurface(std::string&& buffer);

    private:
        Texture       m_font;           ///< Embedded debug console font.
        RenderTexture m_surface;        ///< Surface with printed text.
        Color         m_background;     ///< Cell background color.
        Color         m_foreground;     ///< Cell text color.
        Vector2i      m_cursor;         ///< Current cursor position.
        Vector2i      m_size;           ///< Console size (cells in row and column).
        Vector2f      m_cell;           ///< Single cell size.
        IntRect       m_rect;           ///< Texture rect of current console buffer.
        int           m_scrollOffset;   ///< Current scroll offset.
        int           m_totalHeight;    ///< Total cells in column.
        int           m_height;         ///< Single buffer size.
    };
}

#endif // SFML_ENGINE_CONSOLE_HPP

/*!
    \class sf::Console
    \ingroup engine

    [TODO] Maybe later ... :)
*/
