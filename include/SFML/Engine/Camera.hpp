////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_ENGINE_CAMERA_HPP
#define SFML_ENGINE_CAMERA_HPP

#include <SFML/Engine/Export.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

namespace sf
{
    /*!
        \brief Main camera with virtual resolution and aspect ratio handling.
    */
    class Camera
    {
        friend class Director;

    public:
        /*!
            \brief Enable or disable camera.

            \param enabled True to enable camera.
        */
        void setEnabled(bool enabled);

        /*!
            \brief Set render window for camera.

            This method should not be used in most cases.
            Game loop will set window for us after call initalizer.

            \param window Pointer to window which should use camera.

            \see Engine
        */
        void setRenderWindow(RenderWindow* window);

        /*!
            \brief Set virtual resolution which should be scaled to real window size.

            \param resolution Virtual window size.
        */
        void setVirtualResolution(Vector2f resolution);

        /*!
            \brief Enable or disable keeping aspect ratio.

            This method will keep virtual resolution aspect ratio
            even if real window ratio not match.

            \param keepRatio True to enable aspect ratio handling.
        */
        void keepAspectRatio(bool keepRatio);

        /*!
            \brief Recalculate camera view size.

            This method should not be used in most cases.
            Director handle this for us.
        */
        void update();

        /*!
            \brief Get current virtual resolution.

            \return Virtual window size.
        */
        const Vector2f getVirtualResolution() const;

        /*!
            \brief Check if camera is enabled.

            \return True if camera is enabled, false otherwise.
        */
        bool isEnabled() const;

    private:
        /*!
            \brief Set camera view to render window instance.

            This method should not be used in most cases.
            Director handle this for us.
        */
        void set();

        /*!
            \brief Set default view to render window.

            This method should not be used in most cases.
            Director handle this for us.
        */
        void restore();

    private:
        View            m_view;                      ///> Main camera view.
        RenderWindow*   m_window{ nullptr };         ///> Pointer to render window.
        Vector2f        m_virtualResolution{ 0, 0 }; ///> Virtual screen size.
        bool            m_keepAspectRatio{ false };  ///> Indicate if camera should handle aspect ratio.
        bool            m_needUpdate{ false };       ///> Indicate if camera need update.
        bool            m_enabled{ false };          ///> Indicate if camera is enabled.
    };
}

#endif // SFML_ENGINE_CAMERA_HPP

/*!
    \class sf::Camera
    \ingroup engine

    Class for handle multiple resolutions and keep aspect ratio.
    It's not perfect but should be good for most cases. If you don't
    want to use it, just ignore camera service pointer, because it's
    disabled by default.

    \code
    #include <SFML/Engine.hpp>

    int main()
    {
        sf::Engine::run([](sf::Services& services)
        {
            services.window->create({ 1920, 1080, 32 }, "Window Title" , sf::Style::Fullscreen);

            services.camera->setEnabled(true);
            // All scenes should be fit to virtual resolution.
            // And then camera will scale it to the real window size.
            services.camera->setVirtualResolution({ 1366, 768 });
            // If aspect ratio handling is enabled, black box will be
            // rendered horizontally or vertically dependently of ratio.
            services.camera->keepAspectRatio(false);
        });
    }
    \endcode
*/
