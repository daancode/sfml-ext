////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_ENGINE_REGISTRY_HPP
#define SFML_ENGINE_REGISTRY_HPP

#include <string>
#include <variant>
#include <vector>
#include <unordered_map>

#include <SFML/Engine/Export.hpp>
#include <SFML/Engine/Utils.hpp>

namespace sf
{
    /*!
        \brief Template class. Registry for holding key-value pairs.
    */
    template <typename... _Types>
    class Registry
    {
        using Key = std::string;
        using Value = std::variant<_Types...>;
        using Values = std::unordered_map<std::string, Value>;

    public:
        /*!
            \brief Default constructor;
        */
        Registry() = default;

        /*!
            \brief Deleted copy constructor.
        */
        Registry(const Registry&) = delete;

        /*!
            \brief Deleted move constructor.
        */
        Registry(Registry&&) = delete;

        /*!
            \brief Deleted copy assignment operator.
        */
        Registry& operator=(const Registry&) = delete;

        /*!
            \brief Deleted move assignment operator.
        */
        Registry& operator=(Registry&&) = delete;

        /*!
            \brief Default destructor.
        */
        ~Registry() = default;

        /*!
            \brief Insert or assign new element to group with given key and value.

            \param group Group which key should be assigned
            \param key Key for the assigned value
            \param value Value defined as variant of Registry types
        */
        void set(const Key& group, const Key& key, Value value) noexcept
        {    
            m_data[group][key] = value;
        }

        /*!
            \brief Get existing element or empty value of given type.

            \param group Group for which the given key exist
            \param key Key for the assigned value

            \return Copy of the value from registry
        */
        template <typename _Type>
        _Type get(const Key& group, const Key& key)
        {
            return std::get<_Type>(m_data[group][key]);
        }

        /*!
            \brief Get existing element.

            \param group Group for which the given key exist
            \param key Key for the assigned value
        */
        template <typename _Type>
        void assign(const Key& group, const Key& key, _Type& value)
        {
            value = std::get<_Type>(m_data[group][key]);
        }

        /*!
            \brief Remove element from registry.

            \param group Group for which the given key exist
            \param key Key for the assigned value
        */
        void remove(const Key& group, const Key& key) noexcept
        {
            if( !group.empty() && !key.empty() )
            {
                m_data[group].erase(key);
            }
        }

        /*!
            \brief Remove all elements from group.

            \param group Group for which the given key exist
        */
        void remove(const Key& group) noexcept
        {
            m_data.erase(group);
        }

    private:
        std::unordered_map<Key, Values> m_data; ///< Map for holding values.
    };
}

#endif // SFML_ENGINE_REGISTRY_HPP

/*!
    \class sf::Registry
    \ingroup engine

    This template class help you with holding data in memory.
    It hold data in groups. Each group have own keys and values.

    Important! Registry class will not perform any checks for parameters.

    Usage example:
    \code
        sf::Registry<bool, int, double, std::string> registry;

        registry.set("settings", "width", 1024);
        registry.set("settings", "height", 768);

        sf::VideoMode mode;
        mode.width = registry.get("settings", "width");
        mode.height = registry.get("settings", "height");

        registry.remove("settings");
    \endcode
*/
