////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_ENGINE_LOGGER_HPP
#define SFML_ENGINE_LOGGER_HPP

#include <SFML/Engine/Export.hpp>

#include <array>
#include <sstream>
#include <atomic>
#include <shared_mutex>
#include <future>
#include <iomanip>
#include <vector>

namespace sf
{
    /*!
        \brief Enumeration of the log message types.
    */
    enum class LogType
    {
        Info,       ///< Basic information log.
        Warning,    ///< Warning log.
        Error       ///< Error log type for indicate something is wrong.
    };

    /*!
        \brief Singleton class for manage global log buffer.
    */
    class Logger
    {
        using LogData = std::pair<LogType, std::string>;
        using Buffer = std::array<LogData, 100000>;

    public:
        /*!
            \brief Interface for log watchers.
        */
        class Watcher
        {
            friend class Logger;

        public:
            /*!
                \brief Default destructor.
            */
            virtual ~Watcher() = default;

            /*!
                \brief Pure virtual method for printing log message.

                \param type Log type - Info, Warning or Error
                \param message Message which should be printed
            */
            virtual void print(LogType type, std::string_view message) = 0;

            /*!
                \brief Reset current log id for this watcher.
            */
            void reset();

        private:
            int m_nextId{ 0 };
        };

        using Watchers = std::vector<std::unique_ptr<Watcher>>;

        /*!
            \brief Enable or disable logging.

            \param enabled If true enable logger
        */
        static void setEnabled(bool enabled);

        /*!
            \brief Create single log object.

            \param type Log type - Info, Warning or Error
            \param message String containing log message data
        */
        static void log(LogType type, const std::string& message);

        /*!
            \brief Get log data for given id.

            \param id Log id
        */
        static LogData* get(int id);

        /*!
            \brief Create new watcher.

            \param args Watcher constructor arguments
        */
        template <typename _Type, typename... _Args>
        static void addWatcher(_Args&& ...args)
        {
            auto& instance = getInstance();
            std::unique_lock lock(instance.m_mutex);
            auto watcher = std::make_unique<_Type>(std::forward<_Args>(args)...);
            instance.m_watchers.push_back(std::move(watcher));
        }

        /*!
            \brief Remove all watchers.
        */
        static void removeWatchers();

        /*!
            \brief Update logger state and watchers.
        */
        static void update();

        /*!
            \brief Check if logger is enabled or not.

            \return True if logger is enabled, false otherwise
        */
        static bool isEnabled();

    private:
        /*!
            \brief Default constructor.
        */
        Logger() = default;
        
        /*!
            \brief Deleted copy constructor.
        */
        Logger(const Logger&) = delete;
        
        /*!
            \brief Deleted move constructor.
        */
        Logger(Logger&&) = delete;

        /*!
            \brief Deleted copy assignment operator.
        */
        Logger& operator=(const Logger&) = delete;
        
        /*!
            \brief Deleted move assignment operator.
        */
        Logger& operator=(Logger&&) = delete;

        /*!
            \brief Get global logger instance.

            \return Static instance of logger.
        */
        static Logger& getInstance()
        {
            static Logger instance;
            return instance;
        }

    private:
        std::shared_mutex   m_mutex;            ///< Global mutex.  
        std::atomic<int>    m_currentId{ 0 };   ///< Current log id.
        std::atomic<bool>   m_enabled{ false }; ///< Flag indices if logger is enabled or not.
        Watchers            m_watchers;         ///< Vector of unique Watcher pointers.
        Buffer              m_buffer;           ///< Global log buffer.
    };

    class Log
    {
    public:
        /*!
            \brief Create information log.

            \param args Log message arguments
        */
        template <typename... _Args>
        static void info(_Args&& ...args)
        {
            if( Logger::isEnabled() )
            {
                std::stringstream stream;
                timestamp(stream);
                stream << "[I][Thread:" << std::setw(5) << std::this_thread::get_id() << "] ";
                ( stream << ... << args );
                Logger::log(LogType::Info, stream.str());
            }
        }

        /*!
            \brief Create warning log.

            \param args Log message arguments
        */
        template <typename... _Args>
        static void warning(_Args&& ...args)
        {
            if( Logger::isEnabled() )
            {
                std::stringstream stream;
                timestamp(stream);
                stream << "[W][Thread:" << std::setw(5) << std::this_thread::get_id() << "] ";
                ( stream << ... << args );
                Logger::log(LogType::Warning, stream.str());
            }
        }

        /*!
            \brief Create error log.

            \param args Log message arguments
        */
        template <typename... _Args>
        static void error(_Args&& ...args)
        {
            if( Logger::isEnabled() )
            {
                std::stringstream stream;
                timestamp(stream);
                stream << "[E][Thread:" << std::setw(5) << std::this_thread::get_id() << "] ";
                ( stream << ... << args );
                Logger::log(LogType::Error, stream.str());
            }
        }

    private:
        /*!
            \brief Generate timestamp for log.

            \param stream Stream where timestamp should be printed
        */
        static void timestamp(std::stringstream& stream);
    };
}

#endif // SFML_ENGINE_LOGGER_HPP

/*!
    \class sf::Logger
    \ingroup engine

    [TODO] Maybe later ... :)
*/
