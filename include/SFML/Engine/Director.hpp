////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_ENGINE_DIRECTOR_HPP
#define SFML_ENGINE_DIRECTOR_HPP

#include <SFML/System/NonCopyable.hpp>
#include <SFML/Engine/Scene.hpp>

#include <map>
#include <optional>
#include <memory>
#include <vector>
#include <future>
#include <thread>
#include <set>

namespace sf
{
    /*!
        \brief Scene manager.
    */
    class SFML_ENGINE_API Director : NonCopyable, public Drawable
    {
    public:
        /*!
            \brief Enumeration contains director request types.
        */
        enum RequestType
        {
            Push,           ///< Push scene with given id to stack if it's idle
            PushExclusive,  ///< Push scene as exclusive one if it's idle
            ClearExclusive, ///< Clear exclusive scene and resume stack top
            Pop             ///< Pop current stack top and resume previous
        };

    private:
        //friend class Engine;

        /*!
            \brief Simple struct which connect request type with scene id.
        */
        struct Request
        {
            RequestType type;    ///< Request type from RequestType enum
            std::string sceneId; ///< Existing scene id string
        };

        using SceneId       = std::string;
        using ScenePtr      = std::shared_ptr<Scene>;
        using SceneMap      = std::map<std::string_view, ScenePtr>;
        using SceneStack    = std::vector<ScenePtr>;
        using Requests      = std::vector<Request>;
        using Workers       = std::map<std::string_view, std::future<bool>>;

    public:
        Director() = default;

        ~Director();

        /*!
            \brief Construct new scene with unique id.

            \param id Unique scene id string
            \param args Scene constructor parameters

            \return Self reference
        */
        template <typename _Type, typename... _Args>
        Director& create(const SceneId& id, _Args&& ...args)
        {
            auto scene = std::make_shared<_Type>(std::forward<_Args>(args)...);
            scene->m_id = id;
            m_sceneMap.insert({ scene->m_id, scene });
            return *this;
        }

        /*!
            \brief Destroy scene with given id.

            \param id Unique scene id string
        */
        void destroy(const SceneId& id);

        /*!
            \brief Initialize scene with given id.

            This method call initialize() method on given scene.
            
            \param id Unique scene id string
            \param inBackground Determine if method should be called in separate thread
        */
        void initialize(const SceneId& id, bool inBackground = true);

        /*!
            \brief Release scene with given id.

            This method call release() method on given scene.

            \param id Unique scene id string
            \param inBackground Determine if method should be called in separate thread
        */
        void release(const SceneId& id, bool inBackground = true);

        /*!
            \brief Create new director request.

            \param type Request type
            \param id Unique scene id string
        */
        void request(RequestType type, const std::string& id = "");

        /*!
            \brief Update scene stack.

            This method call onUpdate() method on all scenes on stack
            if scene state is Default or Update.

            \param delta Last frame time
        */
        void update(float delta);

        /*!
            \brief Update scene stack.

            This method call onFixedUpdate() method on all scenes on stack
            if scene state is Default or Update.

            \param delta Fixed (Constant) frame time
        */
        void fixedUpdate(float delta);

        /*!
            \brief Render scene stack.

            This method call draw() method on all scenes on stack
            if scene state is Default or Render.

            \param target Render target to draw to
            \param states Current render states
        */
        void draw(RenderTarget& target, RenderStates states) const;

        /*!
            \brief Check if scene is on top of the stack.

            \param id Unique scene id string

            \return True if scene is on top of the stack
        */
        bool isOnTop(const SceneId& id);

        /*!
            \brief Render scene stack.

            \param id Unique scene id string

            \return Shared pointer to Scene
        */
        ScenePtr get(const SceneId& id);

    private:
        /*!
            \brief Update all existing workers.
        */
        void updateWorkers();

        /*!
            \brief Handle all requests from previous frame.
        */
        void handleRequests();

    private:
        Workers         m_workers;       ///< Workers for initialize/release scene
        SceneMap        m_sceneMap;      ///< All defined scenes
        SceneStack      m_sceneStack;    ///< Current scene stack
        Requests        m_requests;      ///< Requests queue
        ScenePtr        m_exclusive;     ///< Exclusive scene ptr
    };
}

#endif // SFML_ENGINE_DIRECTOR_HPP

/*!
    \class sf::Director
    \ingroup engine

    [TODO] Detailed description ... maybe later :)
*/
