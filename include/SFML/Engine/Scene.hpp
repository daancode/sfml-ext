////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_ENGINE_SCENE_HPP
#define SFML_ENGINE_SCENE_HPP

#include <SFML/Engine/Export.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Engine/Core.hpp>

#include <string>
#include <string_view>

namespace sf
{
    /*!
        \brief Scene interface. Drawable representation of scene with it's own transform.
    */
    class SFML_ENGINE_API Scene : public Drawable, public Transformable
    {
        friend class Director;

    public:
        /*!
            \brief Enumeration of the scene behaviour when it's active.
        */
        enum Mode
        {
            Default,    ///< Scene will be updated and rendered if is active
            OnlyUpdate, ///< If scene is active and not at the top of the stack, it will be only updated
            OnlyRender, ///< If scene is active and not at the top of the stack, it will be only rendered
            Suspended   ///< Oposite to default mode
        };

        /*!
            \brief Enumeration of the states in which scene may be.
        */
        enum State
        {
            Idle,       ///< Default state, scene is not exclusive and not on the stack
            Exclusive,  ///< Scene is exclusive one, cannot be on the stack
            Active      ///< Scene is on stack but may not be on top of it and can't be exclusive
        };

    public:
        /*!
            \brief Default destructor, it's guarantee call destructor of derived classes.
        */
        virtual ~Scene() = default;

        /*!
            \brief Initialize scene, should be called manualy.

            \see Director
        */
        virtual bool initialize() { return false; };

        /*!
            \brief Method called once, when it's pushed on scene stack.
        */
        virtual void onEnter() {};

        /*!
            \brief Method called is always when other scene was pushed to stack.
        */
        virtual void onSuspend() {};

        /*!
            \brief Method called is always when it's again at the top of the stack.
        */
        virtual void onResume() {};

        /*!
            \brief Method called every frame.
        */
        virtual void onUpdate(float delta) {};

        /*!
            \brief Method called every frame with fixed time step.
        */
        virtual void onFixedUpdate(float delta) {};

        /*!
            \brief Method called after pop scene from stack.
        */
        virtual void onExit() {};

        /*!
            \brief Release scene, should be called manually.

            \see Director
        */
        virtual bool release() { return false; };

        /*!
            \brief Set the type of scene behaviour.

            This method defines how scene should behave,
            there are few types of scene state:
                * Default - normal behaviour
                * Update - only update and fixedUpdate methods will be called
                * Render - only render method will be called
                * Suspend - scene will be not updated and rendered

            \param mode New scene mode
        */
        void setMode(Mode mode);

        /*!
            \brief Get the unique scene id.

            \return Unique scene id string
        */
        std::string_view getId() const;

        /*!
            \brief Get current scene state.

            Scene can be in 3 states:
                * Idle - scene is not on stack and not exclusive one
                * Exclusive - only this scene will be updated and rendered
                * Active - scene is on the stack

            \return Current scene state
        */
        State getState() const;

        /*!
            \brief Check if scene is initialized.

            \return Returns true if scene is initialized
        */
        bool isInitialized() const;

    private:
        std::string m_id{ "Scene" };         ///< Unique scene id string.
        State       m_state{ Idle };         ///< Current scene state.
        Mode        m_mode{ Default };       ///< Current scene mode.
        bool        m_initialized{ false };  ///< Flag indices if scene if initialized.
    };
}

#endif // SFML_ENGINE_SCENE_HPP

/*!
    \class sf::Scene
    \ingroup engine

    [TODO] Detailed description ... maybe later :)
*/
