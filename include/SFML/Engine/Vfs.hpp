////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_ENGINE_VFS_HPP
#define SFML_ENGINE_VFS_HPP

#include <SFML/Engine/Export.hpp>
#include <SFML/Graphics.hpp>

#include <array>
#include <memory>

namespace sf
{
    class Vfs
    {
        using Key   = std::string;
        using Path  = std::string;
        using Byte  = unsigned char;
        using Time  = long long;

        /*!
            \brief Struct for describe file.
        */
        struct FileHandle
        {
            Path pak;               ///< Path for pak file.
            Path path;              ///< Path for file.
            std::vector<Byte> file; ///< File data vector.
            Time lastModification;  ///< Last file modification time.
        };

        using FileMap = std::map<std::string_view, std::shared_ptr<FileHandle>>;

    public:
        /*!
            \brief Interface for define encryption method.
        */
        class Cipher
        {
        public:
            /*!
                \brief Default destructor.
            */
            virtual ~Cipher() = default;

            /*!
                \brief Main method for encrypt file data with given key.

                \param data Reference to data vector
                \param key Encryption key
            */
            virtual void encrypt( std::vector<Byte>& data, const Key& key );
        };

    public:
        /*!
            \brief Set encryption method for pak files.

            \param args Cipher class constructor arguments
        */
        template <typename _Type, typename... _Args>
        void setEncryptionMethod(_Args&& ...args)
        {
            m_encryption = std::make_unique<_Type>(std::forward<_Args>(args)...);
        }

        /*!
            \brief Build new pak file from given directory.

            \param path Path to directory which contains asset files
            \param output Result pak path
            \param key Encryption key for build pak
        */
        void build( const Path& path, const Path& output, const Key& key );

        /*!
            \brief Load single file from hard drive.

            \param path Path to file on drive
        */
        void loadFromFile( const Path& path );

        /*!
            \brief Load all files from given directory.

            \param path Path to directory on drive
        */
        void loadFromDirectory( const Path& path );

        /*!
            \brief Load all files from pak file.

            \param path Path to file on drive
            \param key Encryption key for pak file
        */
        void loadFromPak( const Path& path, const Key& key );

        /*!
            \brief Get file handle.

            \param path Path to file on drive

            \return Pointer to handle if is loaded
        */
        std::shared_ptr<const FileHandle> get( const Path& path );

        /*!
            \brief Destroy file handle.

            \param path Path to file
        */
        void release( const Path& path );

    private:
        std::unique_ptr<Cipher> m_encryption{ new Cipher() }; ///< Encryption method for paks.
        FileMap m_fileMap;                                    ///< File handles map.
    };
}

#endif // SFML_ENGINE_VFS_HPP
