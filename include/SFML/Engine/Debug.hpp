////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_ENGINE_DEBUG_HPP
#define SFML_ENGINE_DEBUG_HPP

#include <SFML/Engine/Export.hpp>
#include <SFML/Graphics.hpp>

#include <SFML/Engine/Core.hpp>
#include <SFML/Engine/Scene.hpp>
#include <SFML/Engine/EventHandler.hpp>
#include <SFML/Engine/Console.hpp>
#include <SFML/Engine/Director.hpp>

#include <sstream>
#include <unordered_map>
#include <functional>
#include <memory>

namespace sf::constants
{
    static constexpr auto DebugScene{ "DebugConsole" };
}

namespace sf
{
    /*!
        \brief Debug utility allows to draw debug info on screen and manage debug console.
    */
    class SFML_ENGINE_API Debug : public Drawable
    {
        using Key         = std::string;
        using Arguments   = std::vector<Key>;
        using Callback    = std::function<void(const Arguments&)>;
        using CommandMap  = std::unordered_map<Key, Callback>;
        using DirectorPtr = std::weak_ptr<Director>;

    public:
        /*!
            \brief Debug console scene.
        */
        class SFML_ENGINE_API CommandLine : public Scene
        {
        public:
            /*!
                \brief Create console adjusted to current window.

                \param debug Pointer to debug utility class

                \return True if initialization completed succesfully
            */
            bool initialize() override;

            /*!
                \brief Clear current command.
            */
            void onEnter() override;

            /*!
                \brief Update console buffer and process commands.

                \param delta Current frame time
            */
            void onUpdate(float delta) override;

            /*!
                \brief Draw debug console on screen.

                \param target Render target
                \param states Current render states
            */
            void draw(RenderTarget& target, RenderStates states) const override;

            /*!
                \brief Print message on console screen.

                \param message Message to print
            */
            void print(const std::string& message);

            /*!
                \brief Print message on console screen.

                \param message Message to print
                \param color Message text color
            */
            void print(const std::string& message, Color color);

        private:
            RectangleShape  m_background;   ///< Debug console background.
            Console         m_cmd;          ///< Command line representation.
            Console         m_buffer;       ///< Debug console buffer.
            std::string     m_cmdBuffer;    ///< Command line data.
            size_t          m_totalCmdSize; ///< Total command line size.
        };

        /*!
            \brief Implementation of log watcher for debug console.
        */
        class Watcher : public Logger::Watcher
        {
        public:
            /*!
                \brief Class constructor.

                \param console Pointer to debug console scene.
            */
            Watcher(CommandLine* console);

            /*!
                \brief Print logs to debug console scene.

                \param type Log type - Info, Warning or Error
                \param message Message which should be printed
            */
            void print(sf::LogType type, std::string_view message) override;

        private:
            CommandLine * m_console{ nullptr }; ///< Pointer to debug console scene.
        };

        /*!
            \brief Enumeration of the debug mode.
        */
        enum class Mode
        {
            Overlay, ///< Display debug text on screen
            Console  ///< Push debug console as exclusive scene
        };

    public:
        /*!
            \brief Initialize debug overlay and create console.
        */
        void init();

        /*!
            \brief Check if debug mode is enabled.

            \return True if debug mode is enabled, false otherwise
        */
        bool isEnabled() const;

        /*!
            \brief Enable/disable debug mode.

            \param enabled True to enable, false to disable
        */
        void setEnabled(bool enabled);

        /*!
            \brief Set debug mode to overlay or console.

            \param mode Set debug mode
        */
        void setMode(Mode mode);

        /*!
            \brief Print string on top of all scenes.

            \param pos Cell where string should be printed
            \param string Data to draw on overlay
        */
        void print(const Vector2i& pos, const std::string& string);

        /*!
            \brief Draw overlay on screen if enabled.

            \param target Render target
            \param states Current render states
        */
        void draw(RenderTarget& target, RenderStates states) const;

        /*!
            \brief Clear overlay data.
        */
        void clear();

        /*!
            \brief Map callback function to command.

            \param cmd Command string
            \param callback Function to call after command execution
        */
        void addCommand(const Key& cmd, Callback callback);

        /*!
            \brief Call command callback if exist.

            \param cmd Command string
        */
        bool executeCommand(const Key& cmd);

        /*!
            \brief Remove command from command map.

            \param cmd Command string
        */
        void removeCommand(const Key& cmd);

        /*!
            \brief Remove command from command map.

            \return Pointer to debug console scene.
        */
        CommandLine* getConsoleScene();

    private:
        Vector2u    m_size;                  ///< Current window size (in cells)
        Console     m_overlay;               ///< Overlay console to draw info
        CommandMap  m_cmdMap;                ///< All commands with callbacks
        Mode        m_mode{ Mode::Overlay }; ///< Current mode
        bool        m_enabled{ false };      ///< Enable debug utils
    };
}

#endif // SFML_ENGINE_DEBUG_HPP

/*!
    \class sf::Debug
    \ingroup engine

    [TODO] Maybe later ... :)
*/
