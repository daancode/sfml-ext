////////////////////////////////////////////////////////////
//
// SFML-EXT - Game development extenstion for SFML
// Copyright (C) 2018 Damian Barczynski (kontakt.daan@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_ENGINE_HPP
#define SFML_ENGINE_HPP

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////

#include <SFML/Graphics.hpp>
#include <SFML/Engine/Scene.hpp>
#include <SFML/Engine/Director.hpp>
#include <SFML/Engine/EventHandler.hpp>
#include <SFML/Engine/Core.hpp>
#include <SFML/Engine/Registry.hpp>
#include <SFML/Engine/Console.hpp>
#include <SFML/Engine/Debug.hpp>
#include <SFML/Engine/Camera.hpp>
#include <SFML/Engine/AnimationPrefab.hpp>
#include <SFML/Engine/Animation.hpp>

#endif // SFML_ENGINE_HPP


////////////////////////////////////////////////////////////
/// \defgroup engine Engine module
///
/// Game development extension, scene management, assets etc.
///
////////////////////////////////////////////////////////////
